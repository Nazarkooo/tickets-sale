const express = require("express");
const cancelRouter = express.Router();
const Ticket = require("./models/ticket");

cancelRouter.post("/:ticketID", async (req, res) => {
  try {
    const order = await Ticket.updateOne(
      { _id: req.query.id },
      { $set: { isTaken: false } }
    );
    res.json(order);
  } catch (error) {
    res.json({ message: error });
  }
});

module.exports = cancelRouter;
