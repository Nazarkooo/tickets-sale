const express = require("express");
const orderRouter = express.Router();
const Ticket = require("./models/ticket");

orderRouter.post("/", async (req, res) => {
  try {
    const order = await Ticket.updateOne(
      { _id: req.query.id },
      { $set: { isTaken: true } }
    );
    res.json(order);
  } catch (error) {
    res.json({ message: error });
  }
});

module.exports = orderRouter;
