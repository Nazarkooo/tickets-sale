const mongoose = require("mongoose");

const TicketsScheme = mongoose.Schema({
  number: { type: Number, required: true },
  sector: { type: String, required: true },
  isTaken: { type: Boolean, required: true },
});

module.exports = mongoose.model("tickets", TicketsScheme);
