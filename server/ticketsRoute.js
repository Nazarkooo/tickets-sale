const express = require("express");
const ticketsRouter = express.Router();
const Ticket = require("./models/ticket");

ticketsRouter.get("/", async (req, res) => {
  try {
    const tickets = await Ticket.find();
    res.json(tickets);
  } catch (error) {
    res.json({ message: error });
  }
});

ticketsRouter.post("/", async (req, res) => {
  const newTicket = new Ticket({
    number: req.query.number,
    sector: req.query.sector,
    isTaken: false,
  });

  try {
    const savedTicket = await newTicket.save();
    res.json(savedTicket);
  } catch (error) {
    res.json({ message: error });
  }
});

module.exports = ticketsRouter;
