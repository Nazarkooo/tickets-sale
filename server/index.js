const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const ticketsRouter = require("./ticketsRoute");
const orderRouter = require("./orderRoute");
const cancelRouter = require("./cancelRoute");

const startServer = () => {
  try {
    mongoose.connect(
      "mongodb+srv://n1lkey:7777@ticketssale.qoorkhx.mongodb.net/?retryWrites=true&w=majority",
      { useNewUrlParser: true }
    );

    app.use(cors());
    app.use("/tickets", ticketsRouter);
    app.use("/order", orderRouter);
    app.use("/cancel", cancelRouter);

    app.listen(3000, () => console.log("Server started"));
  } catch (error) {
    console.log(`Error - ${error}`);
  }
};
startServer();
