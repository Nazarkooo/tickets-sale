import React, { useState, useEffect } from "react";
import { AiTwotonePhone, AiTwotoneMail } from "react-icons/Ai";
import axios from "axios";
import Sidebar from "./Sidebar";

export default function Tickets() {
  const [tickets, setTickets] = useState([]);
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);
  const [sector, setSector] = useState("");

  useEffect(() => {
    axios
      .get("http://localhost:3000/tickets")
      .then(function (response) {
        setTickets(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  const openSidebarHandler = (sector) => {
    setIsSidebarOpen(true);
    setSector(sector);
  };

  return (
    <div className="main-wrapper">
      {isSidebarOpen && (
        <Sidebar
          setIsSidebarOpen={setIsSidebarOpen}
          sector={sector}
          tickets={tickets}
          setTickets={setTickets}
        />
      )}
      <header>
        <div className="wrapper">
          <a href="">
            <img src="./src/assets/images/logo.png" alt="" />
          </a>
          <nav>
            <ul className="navbar-list">
              <li className="navbar-list-item">Колл-центр з 10:00 - 19:00</li>
              <li className="navbar-list-item">
                <a href="tel:+380635107727">
                  <AiTwotonePhone />
                </a>
              </li>
              <li className="navbar-list-item">
                <a href="mailto:ticketifysell@gmail.com">
                  <AiTwotoneMail />
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </header>

      <main>
        <div className="wrapper">
          <div className="sectors-btn">
            {[...new Set(tickets.map((el) => el.sector))].map((el) => (
              <button key={el} onClick={() => openSidebarHandler(el)}>
                {el}
              </button>
            ))}
          </div>
          <img
            className="stadium-img"
            src="./src/assets/images/stadium.jpg"
            alt=""
          />
        </div>
      </main>
    </div>
  );
}
