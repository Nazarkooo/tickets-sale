import React from "react";
import ReactDOM from "react-dom/client";
import "./index.scss";
import Tickets from "./Tickets";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <Tickets />
  </React.StrictMode>
);
