import axios from "axios";
import React, { useState } from "react";
import { AiOutlineClose } from "react-icons/Ai";

export default function Sidebar({
  setIsSidebarOpen,
  sector,
  tickets,
  setTickets,
}) {
  const [ticketId, setTicketId] = useState(null);

  const ticketsBySector = tickets.filter((el) => el.sector === sector);

  const orderPlaceHandler = () => {
    axios
      .post(`http://localhost:3000/order?id=${ticketId}`)
      .then((response) => {
        if (response.data.modifiedCount > 0) {
          setTickets(
            tickets.map((el) => {
              if (el._id === ticketId) {
                el.isTaken = true;
              }
              return el;
            })
          );
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const cancelPlaceHandler = () => {};

  return (
    <div className="sidebar">
      <p>Order Ticket - {sector}</p>
      <button
        onClick={() => setIsSidebarOpen(false)}
        className="close-sidebar-btn"
      >
        <AiOutlineClose />
      </button>
      <div className="sector-places">
        {ticketsBySector.map((el) =>
          el.isTaken ? (
            <button
              key={el._id}
              onClick={() => cancelPlaceHandler(el._id)}
              className="sector-places-numbers red"
            >
              {el.number}
            </button>
          ) : (
            <button
              key={el._id}
              onClick={() => setTicketId(el._id)}
              className="sector-places-numbers green"
            >
              {el.number}
            </button>
          )
        )}
      </div>

      {ticketId ? (
        <button onClick={orderPlaceHandler} className="order-btn">
          Order
        </button>
      ) : (
        <button onClick={orderPlaceHandler} className="order-btn" disabled>
          Order
        </button>
      )}
    </div>
  );
}
